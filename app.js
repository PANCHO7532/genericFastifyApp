const fastify = require("fastify");
const pug = require("pug");
const path = require("path");
const app = fastify();
const host = process.env.HOST || '0.0.0.0';
const port = process.env.PORT || 3000;
app.register(require("@fastify/cookie"));
app.register(require("@fastify/formbody"));
app.register(require("@fastify/static"), { root: path.join(__dirname, 'public') });
app.register(require("@fastify/static"), {
    root: path.join(__dirname, 'node_modules/bootstrap/dist'),
    prefix: "/bootstrap",
    decorateReply: false
});
app.register(require("@fastify/view"), {
    engine: { pug },
    root: path.join(__dirname, "views"),
    propertyName: "render"
});
app.register(require("./routes"));
app.listen({port, host}, (err, addr) => {
    if(err) {
        console.log("[ERROR] An error occurred!");
        app.log.error(err);
        process.exit(1);
    }
    console.log("[INFO] Server started");
});
