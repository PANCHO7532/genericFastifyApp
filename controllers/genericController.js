const pageTitle = "GenericFastifyApp";
exports.index = async(req, res) => {
    res.clearCookie("exampleFastifyAppData");
    await res.render("index", {
        title: pageTitle,
    });
};
exports.post = async(req, res) => {
    res.cookie("exampleFastifyAppData", req.body.content, {maxAge: 3600});
    res.redirect("/page");
}
exports.page = async(req, res) => {
    await res.render("page", {
        title: pageTitle,
        cookie: req.cookies.exampleFastifyAppData
    });
}