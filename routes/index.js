const genericController = require("../controllers/genericController");
module.exports = function(app, options) {
    app.get("/", genericController.index);
    app.get("/page", genericController.page);
    app.post("/post", genericController.post);
    return app;
}